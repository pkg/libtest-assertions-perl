libtest-assertions-perl (1.054-5+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 16:54:05 +0000

libtest-assertions-perl (1.054-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtest-assertions-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 17:28:43 +0000

libtest-assertions-perl (1.054-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 11:50:46 +0100

libtest-assertions-perl (1.054-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:41:27 +0200

libtest-assertions-perl (1.054-3apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:30:43 +0200

libtest-assertions-perl (1.054-3) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Simplify debian/rules file
  * Drop comment line from debian/watch
  * Add debian/upstream/metadata
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6
  * Refresh license description in debian/copyright
  * Add debian/source/format
  * Mark package as autopkg-testable

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Tue, 22 Dec 2015 12:03:16 -0200

libtest-assertions-perl (1.054-2) unstable; urgency=low

  [ David Paleino ]
  * debian/control: removed myself from Uploaders (Closes: #509508)

  [ gregor herrmann ]
  * debian/control:
    - added: Vcs-Svn field (source stanza); Vcs-Browser
      field (source stanza). Removed: XS-Vcs-Svn fields
    - added: ${misc:Depends} to Depends: field
    - set Standards-Version to 3.8.0 (no changes)
    - add /me to Uploaders
  * Don't install README anymore.
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * debian/copyright: switch to new format.

 -- gregor herrmann <gregoa@debian.org>  Wed, 07 Jan 2009 17:18:59 +0100

libtest-assertions-perl (1.054-1) unstable; urgency=low

  WAITING FOR liblog-trace-perl

  * Initial Release.

 -- David Paleino <d.paleino@gmail.com>  Tue, 18 Sep 2007 12:28:35 +0200
